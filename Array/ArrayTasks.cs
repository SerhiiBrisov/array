﻿using System;
using System.Linq;

namespace ArrayObject
{
    public static class ArrayTasks
    {
        /// <summary>
        /// Task 1
        /// </summary>
        public static void ChangeElementsInArray(int[] nums)
        {
            for (int i = 0; i < nums.Length / 2; i++)
            {
                if (nums[i] % 2 == 0 && nums[nums.Length - i - 1] % 2 == 0)
                {
                    // Hint: Don`t create or initialize any array, work only with given 'numbs'

                    // declare some local variables
                    int temp = nums[i];
                    nums[i] = nums[nums.Length - i - 1];
                    nums[nums.Length - i - 1] = temp;
                }
            }
        }

        /// <summary>
        /// Task 2
        /// </summary>
        public static int DistanceBetweenFirstAndLastOccurrenceOfMaxValue(int[] nums)
        {
            if (nums.Length == 0)
            {
                return 0;
            }
            int max = nums.Max();
            int firstOccurence = 0;
            int lastOccurence = 0;
            bool flag = false;
            for (int i = 0; i < nums.Length; i++)
            {
                if (nums[i] == max && !flag)
                {
                    firstOccurence = i;
                    flag = true;
                }
                else
                {
                    if (nums[i] == max)
                    {
                        lastOccurence = i;
                    }
                }
            }
            int res = 0;
            if (lastOccurence == 0)
            {
                return res;
            }
            else
            {
                res = lastOccurence - firstOccurence;
                return res;
            }
        }

        /// <summary>
        /// Task 3 
        /// </summary>        
        public static void ChangeMatrixDiagonally(object o)
        {
            int[,] matrix = o as int[,];

            for (int i = 0; i < Math.Sqrt(matrix.Length); i++)
            {
                for (int j = 0; j < Math.Sqrt(matrix.Length); j++)
                {
                    if (i > j)
                    {
                        matrix[i, j] = 0;
                    }
                    else
                    {
                        if (i < j)
                        {
                            matrix[i, j] = 1;
                        }
                    }
                }
            }
        }
    }
}
